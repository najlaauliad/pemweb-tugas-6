<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mhs_225150409111007 extends Model
{
    use HasFactory;
    protected $table = 'mhs_225150409111007';
    protected $fillable = ['nim', 'nama'];
}
