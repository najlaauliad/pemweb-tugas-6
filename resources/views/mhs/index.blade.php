    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <div class="card">
                    <h3 align="center">Tugas 6 Najla' Aulia Dewi</h3>
                    <div class="card-header">Data Mahasiswa</div <div class="card-body">
                    <a href="{{ route('mhs.create') }}" class="btn btn-primary mb-3">Tambah Data</a>
                    <table class="table" border="1">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($mhs as $m)
                            <tr>
                                <td>{{ $m->id }}</td>
                                <td>{{ $m->nim }}</td>
                                <td>{{ $m->nama }}</td>
                                <td>{{ $m->created_at }}</td>
                                <td>{{ $m->updated_at }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>